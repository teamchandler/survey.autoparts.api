//<editor-fold desc="Variable Declaration">

var error_handler = require('./error').errorHandler,
    master_handler = require('./master'),
    express = require('express'),
    util = require('../util.js'),
    repo = require('../repo')
    ;


var logger = util.get_logger("server");
// define routers (new in Express 4.0)
var ping_route = express.Router(),
    master_route = express.Router(),
    help_route = express.Router(),
    login_route = express.Router()
    ;

var token = require('../token');

//</editor-fold>


module.exports = exports = function (app, corsOptions) {
    //<editor-fold desc="Fast Phase">
    // protect any route starting with "master" with validation
    app.all("/master/*", authenticate, function (req, res, next) {
        next(); // if the middleware allowed us to get here,
        // just move on to the next route handler
    });

    // implement ping_route actions
    // This is specific way to inject code when this route is called
    // This executes for any ping route.
    // Similarly we can (and should) implement same for every route
    ping_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
        // continue doing what we were doing and go to the route
        next();
    });
    ping_route.get('/', function (req, res) {
        res.send({'error': 0, 'msg': 'audit endpoints ready for data capture'});
    });
    ping_route.get('/check/mongo', function (req, res) {
        res.send({'error': 0, 'msg': 'Code for checking Mongo connection will be implemented here'});
    });
    ping_route.post('/check/post', function (req, res) {
        res.send('Checking Post Method' + req.body);
    });
    app.use('/ping', ping_route);
    // end ping route

    // master route implementation
    master_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "master", "MASTER"));
        // continue doing what we were doing and go to the route
        next();
    });

    var api_master = master_handler.api_master;

    login_route.get('/test/api', function (req, res) {
        api_master.test_api(req, res);
    });

    //user login
    login_route.post('/user/login', function (req, res) {
        api_master.user_login(req, res);
    });

    //new user creation
    login_route.post('/user/registration', function (req, res) {
        api_master.user_registration(req, res);
    });



//get all retailer details data
    login_route.post('/get/all/retailer/details', function (req, res) {
        api_master.get_all_retailer_details(req, res);
    });


    //new retailer insert
    login_route.post('/new/retailer/insert', function (req, res) {
        api_master.new_retailer_insert(req, res);
    });

    //Update retailer data
    login_route.post('/retailer/data/update', function (req, res) {
        api_master.retailer_data_update(req, res);
    });
//Shalini api to retailer save details


    login_route.post('/save/retailer/details', function (req, res) {
        api_master.save_retailer_details(req, res);
    });
    //get callers screen data
    login_route.post('/get/tab/content', function (req, res) {
        api_master.get_callers_screen_data(req, res);
    });

    //complete retailer call
    login_route.post('/retailer/call/complete', function (req, res) {
        api_master.complete_retailer_call(req, res);
    });


    //shalini api  creating caller api
    login_route.post('/caller/insert/details', function (req, res) {
        api_master.insert_user_details(req, res);
    });
    login_route.post('/get/exsisting/caller/details', function (req, res) {
        api_master.get_exsisting_data_caller(req, res);
    });

    login_route.post('/insert/super/admin/question', function (req, res) {
        api_master.insert_super_admin_question(req, res);
    });

    login_route.get('/get/question/display', function (req, res) {
        api_master.get_feedback_question(req, res);
    });

    //complete retailer survey
    login_route.post('/retailer/survey/update', function (req, res) {
        api_master.survey_data_update(req, res);
    });

    //Update retailer data
    login_route.post('/get/retailer/data', function (req, res) {
        api_master.get_retailer_data(req, res);
    });


    login_route.post('/dont/instered/retailer', function (req, res) {
        api_master.not_interested_data(req, res);
    });




    //Search by input text box
    login_route.post('/get/data/by/search', function (req, res) {

        api_master.get_data_by_search_input(req, res);
    });
    //get wip user list
    login_route.get('/get/wip/user/list', function (req, res) {
        api_master.get_wip_user_list(req, res);
    });

    //get wip user list
    login_route.post('/get/wip/user/data', function (req, res) {
        api_master.get_wip_user_data(req, res);
    });


    // implement help_route actions
    help_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
        // continue doing what we were doing and go to the route
        next();
    });
    help_route.get('/', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(help_route.stack, r_list, "help");
        res.send(r_list);
    });
    help_route.get('/show', function (req, res) {
        var r_list = [];
        get_routes(ping_route.stack, r_list, "ping");
        get_routes(help_route.stack, r_list, "help");
        //res.send(r_list);
        console.log(r_list);
        res.render('show', {'routes': r_list});
    });
    app.use('/help', help_route);
    // end help route

    //implement login_route action
    login_route.use(function (req, res, next) {
        logger.info(util.create_routing_log(req.method, req.url, "login", "LOGIN"));
        // continue doing what we were doing and go to the route
        next();
    });


//  Login API Block End
    app.use('/login', login_route);
    app.use('/master', master_route);
    app.use('/help', help_route);

    //end login_route

    // This will be refactored later for adding more features and making it more generic
    var get_routes = function (r, r_list, route_sub_system) {
        for (var i = 0; i < r.length; i++) {
            if (typeof r[i].route != "undefined") {

                r_list.push(
                    {
                        'path': "/" + route_sub_system + r[i].route.path,
                        'method': r[i].route.methods
                    }
                )
            }
        }
        return r_list;
    };

    // and it will validate based on registered data
    function authenticate(req, res, next) {
        var token_data = token.verify(req.headers.token);
        if (token_data) {
            next();
        } else {

            res.send('Not authorised');
        }
    }

    // Error handling middleware
    app.use(error_handler);

    //</editor-fold>

};