/**
 * Created with JetBrains WebStorm.
 * User: Mahiruddin Seikh
 * Date: 18/10/16
 * Time: 11:35 AM
 * To change this template use File | Settings | File Templates.
 */

var util = require('../../util.js');
var token = require('../../token.js');
var logger = util.get_logger("api");
var _ = require('lodash-node');
var mongodb = require('mongodb');
var uuid = require('node-uuid');
var shortid = require('shortid');
var unirest = require("unirest");
var fs = require('fs');
var knox = require('knox');
var async = require('async');

var serverDB;
//We need to work with "MongoClient" interface in order to connect to a mongodb server.
var MongoClient = mongodb.MongoClient;
// Connection URL. This is where your mongodb server is running.
var url = util.get_connection_string("mongo");
var val = 0;
//Open the Connection of MongoDB to carry out operations
MongoClient.connect(url, function (err, db) {
    if (err) {
        logger.info("Unable To Connect To The MongoDB Server. Error:" + err);
        serverDB = null;
    }
    else {
        //Database has been connected.
        logger.info("Connection Established To MongoDB");
        serverDB = db;
    }
});

//test API
exports.test_api = function (req, res) {

    console.log('Test Passed Successfully');

    util.send_to_response({
        response_code: 1,
        msg: 'Test Passed Successfully'
    }, res);

};

//Login For user Start
exports.user_login = function (req, res) {

    var user_id = req.body.user_id;
    var password = req.body.password;

    if (typeof user_id === 'undefined' || user_id == "" || user_id == "") {
        logger.info("User Id Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'User Id Is Not Supplied'}, res);
        return;
    }

    if (typeof password === 'undefined' || password == "" || password == "") {
        logger.info("Password Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Password Is Not Supplied'}, res);
        return;
    }

    logger.info("Incoming Body : " + JSON.stringify(req.body));
    var retData = {};
    retData.msg = 'Log In Incorrect';
    retData.token = "";

    //Initialize User Collection
    var col_user = serverDB.collection('user');

    //Create User Query
    var query_user = {"user_id": req.body.user_id};

    logger.info("User Search Query : " + JSON.stringify(query_user));

    //Project User Data
    var proj_user = {
        _id: 0,
        user_id: 1,
        user_type: 1,
        user_first_name: 1,
        user_middle_name: 1,
        user_last_name: 1,
        user_designation: 1,
        user_password: 1,
        user_registed_no: 1,
        user_email_id: 1,
        created_by: 1
    };

    col_user.find(query_user, proj_user).toArray(function (err, user_result) {
        if (err) {
            logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching User Data.', error_msg: err}, res);
        }
        else {
            logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);

            var retData = {};
            retData.msg = 'Log In Incorrect';
            retData.token = "";

            if (user_result.length == 1) {
                if (user_result[0].user_password == req.body.password) {
                    var obj_disti = user_result[0];
                    delete obj_disti.user_password;
                    retData.user_deatil = obj_disti;
                    retData.msg = 'Log In Success';
                    retData.response_code = 1;
                    retData.token = token.assign({user_id: req.body.user_id});
                    util.send_to_response(retData, res);
                }
                else {
                    retData.msg = 'Password is Incorrect';
                    retData.response_code = 3;
                    util.send_to_response(retData, res);
                }
            }
            else {
                retData.msg = 'User Id is Incorrect';
                retData.response_code = 2;
                util.send_to_response(retData, res);
            }
        }
    });
};
//Login For user End

//New user registration start
exports.user_registration = function (req, res) {

    var user_id = req.body.user_id;
    var password = req.body.user_password;

    if (typeof user_id === 'undefined' || user_id == "" || user_id == "") {
        logger.info("User Id Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'User Id Is Not Supplied'}, res);
        return;
    }

    if (typeof password === 'undefined' || password == "" || password == "") {
        logger.info("Password Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Password Is Not Supplied'}, res);
        return;
    }

    //Initialize User Collection
    var col_user = serverDB.collection('user');

    //Create User Query
    var query_user = {"user_id": req.body.user_id};

    req.body.created_dt = new Date();

    col_user.find(query_user).toArray(function (err, user_result) {
        if (err) {
            //logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching User Data.', error_msg: err}, res);
        }
        else {
            //logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);
            if (user_result.length > 0) {
                util.send_to_response({response_code: 2, msg: 'User Already Registered.'}, res);
            }
            else {
                //inserting new user in database(user table)
                col_user.insert(req.body, {w: 1, keepGoing: true}, function (err, result) {
                    if (err) {
                        //logger.info("Error in Inserting user,err:" + err.message);
                        util.send_to_response({response_code: 0, msg: 'User Not Registered.'}, res);
                    } else {
                        //logger.info("User Registered Successfully.");
                        util.send_to_response({response_code: 1, msg: 'User Registered Successfully.'}, res);
                    }
                });
            }
        }
    });
};
//New user registration End
//GET ALL  Retailer Details


exports.get_all_retailer_details = function (req, res) {

    var col_retailer_master = serverDB.collection('retailer_master');
    console.log("retailer" + req.body.retailer_code);

    //Create User Query
    var query_user = {"retailer_code": req.body.retailer_code};

    col_retailer_master.find(query_user).toArray(function (err, result) {
        if (err) {
            console.log("err")
        }
        else {
            console.log("result" + JSON.stringify(result));
            util.send_to_response(result, res)
        }

    })


}
//New user registration start
exports.new_retailer_insert = function (req, res) {

    var retailer_registered_mobile = req.body.retailer_registered_mobile;

    if (typeof retailer_registered_mobile === 'undefined' || retailer_registered_mobile == "" || retailer_registered_mobile == "") {
        logger.info("User Id Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Please Supply The Mobile Number.'}, res);
        return;
    }

    //Initialize User Collection
    var col_user = serverDB.collection('retailer_master');

    //Create User Query
    var query_user = {"retailer_registered_mobile": req.body.retailer_registered_mobile};

    req.body.created_dt = new Date();

    col_user.find(query_user).toArray(function (err, retailer_result) {
        if (err) {
            //logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching User Data.', error_msg: err}, res);
        }
        else {
            //logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);
            if (retailer_result.length > 0) {
                util.send_to_response({
                    response_code: 2,
                    msg: 'Retailer Already Registered With This Mobile Number.'
                }, res);
            }
            else {
                //inserting new user in database(user table)
                col_user.insert(req.body, {w: 1, keepGoing: true}, function (err, result) {
                    if (err) {
                        //logger.info("Error in Inserting user,err:" + err.message);
                        util.send_to_response({response_code: 0, msg: 'Retailer Not Inserted.'}, res);
                    } else {
                        //logger.info("User Registered Successfully.");
                        util.send_to_response({response_code: 1, msg: 'Retailer Registered Successfully.'}, res);
                    }
                });
            }
        }
    });
};
//New user registration End
exports.not_interested_data = function (req, res) {
    var col_retailer_master = serverDB.collection('retailer_master');
    var retailer_incoming_data = req.body;
    console.log("incoming body" + JSON.stringify(req.body));

    delete req.body._id;

    var set_qry = {
        "retailer_code": retailer_incoming_data.retailer_code,
        "outlet_name": retailer_incoming_data.outlet_name,
        "retailer_adress": retailer_incoming_data.retailer_adress,
        "pincode": retailer_incoming_data.pincode,
        "retailer_area": retailer_incoming_data.retailer_area,
        "retailer_city": retailer_incoming_data.retailer_city,
        "retailer_state": retailer_incoming_data.retailer_state,
        "retailer_registered_mobile": retailer_incoming_data.retailer_registered_mobile,
        "emailid": retailer_incoming_data.emailid,
        "address_confirmed": retailer_incoming_data.address_confirmed,
        "remarks": retailer_incoming_data.remarks,
        "survey_auto_status": "Complete",
        "call_type": "Retailer Loyalty Survey",
        "call_status": "Survey Done", "active" : 1,
        "retailer_serial_no" : retailer_incoming_data.retailer_serial_no||"",
        "retailer_name" : retailer_incoming_data.retailer_name||"",
        "retailer_region" : retailer_incoming_data.retailer_region||"",
        "retailer_address" : retailer_incoming_data.retailer_address||"",
        "retailer_district" : retailer_incoming_data.retailer_district||"",
        "retailer_pin" :retailer_incoming_data.retailer_pin||"",
        "std_code" :retailer_incoming_data.std_code||"",
        "landmark" :retailer_incoming_data.landmark||"",
        "locality" :retailer_incoming_data.locality||"",

        "alt_contact" :retailer_incoming_data.alt_contact||"",
        "business_anniversary" : retailer_incoming_data.business_anniversary||"",
        "no_of_children" :retailer_incoming_data.no_of_children||"",
        "marriage_anniversary" : retailer_incoming_data.marriage_anniversary||"",
        "age_of_children" :retailer_incoming_data.age_of_children||"",
        "preferred_language" :retailer_incoming_data.preferred_language||"",
        "mobile_os" :retailer_incoming_data.mobile_os||"",
        "birthday" :retailer_incoming_data.birthday||"",
        "survey_date" : new Date()
    }

    // $scope.retailer_data.call_status=="Do Not Want To Participate"||$scope.retailer_data.call_status=="Wrong Number"||$scope.retailer_data.call_status=="No Shop"||$scope.retailer_data.call_status=="Invalid Shop"

    col_retailer_master.update({retailer_code: req.body.retailer_code}, set_qry, {multi: false}, function (err, result) {
        if (err) {
            logger.info("Error in Updating Retailer Data,err:" + err.message);
            util.send_to_response({
                result_value: 0,
                msg: 'Error in Updating Retailer Data',
                err: err
            }, res);
        } else {

            logger.info("New Retailer Data Updated Successfully.");
            complete_retailer_call(retailer_incoming_data, function (err, result) {
                if (result == "Updated  Sucesfully" || result == "Inserted Sucessfully") {
                    util.send_to_response({
                        response_code: 2,
                        msg: 'New Retailer Data Updated Successfully.'
                    }, res);
                }
                else {
                    util.send_to_response({
                        response_code: 2,
                        msg: 'Server Error'
                    }, res);
                }

            })
            // util.send_to_response({
            //     response_code: 2,
            //     msg: 'New Retailer Data Updated Successfully.'
            // }, res);

        }

    })
}

//REtailer Save details

exports.save_retailer_details = function (req, res) {
    // console.log("incoming body"+JSON.stringify(req.body));
    var col_retailer_master = serverDB.collection('retailer_master');
    var retailer_incoming_data = req.body;
    console.log("incoming body" + JSON.stringify(req.body));

    delete req.body._id;

    req.body.survey_date = new Date();


    var retailer_data = {$set: req.body};

    col_retailer_master.update({retailer_code: req.body.retailer_code}, retailer_data, {multi: false}, function (err, result) {
        if (err) {
            logger.info("Error in Updating Retailer Data,err:" + err.message);
            util.send_to_response({
                result_value: 0,
                msg: 'Error in Updating Retailer Data',
                err: err
            }, res);
        } else {

            logger.info("New Retailer Data Updated Successfully.");
            complete_retailer_call(retailer_incoming_data, function (err, result) {
                if (result == "Updated  Sucesfully" || result == "Inserted Sucessfully") {
                    util.send_to_response({
                        response_code: 2,
                        msg: 'New Retailer Data Updated Successfully.'
                    }, res);
                }
                else {
                    util.send_to_response({
                        response_code: 2,
                        msg: 'Server Error'
                    }, res);
                }

            })
            // util.send_to_response({
            //     response_code: 2,
            //     msg: 'New Retailer Data Updated Successfully.'
            // }, res);

        }
    });

}
//Update retailer data start
// exports.retailer_data_update = function (req, res) {
//     //Initialize retailer_master Collection
//     var col_retailer_master = serverDB.collection('retailer_master');
//     console.log("incoming body" + JSON.stringify(req.body));
//
//     delete req.body._id;
//
//     req.body.survey_date = new Date();
//
//     var retailer_data = {$set: req.body};
//
//     col_retailer_master.update({retailer_code: req.body.retailer_code}, retailer_data, {multi: false}, function (err, result) {
//         if (err) {
//             logger.info("Error in Updating Retailer Data,err:" + err.message);
//             util.send_to_response({
//                 result_value: 0,
//                 msg: 'Error in Updating Retailer Data',
//                 err: err
//             }, res);
//         } else {
//
//             logger.info("New Retailer Data Updated Successfully.");
//             complete_retailer_call(retailer_data,function (err,result)
//             {
//
//
//             })
//             // util.send_to_response({
//             //     response_code: 2,
//             //     msg: 'New Retailer Data Updated Successfully.'
//             // }, res);
//
//         }
//     });
//
//
// };

//Added by shalini to insert the calling details
function complete_retailer_call(retailer_data, callback) {
    // console.log("incoming body" + JSON.stringify(retailer_data));
    console.log("insert into call log" + JSON.stringify(retailer_data.retailer_code));
    var col_call_logs = serverDB.collection('call_logs');
    // console.log("incoming body" + JSON.stringify(retailer_data));
    var match_qry = {"retailer_code": retailer_data.retailer_code};
    col_call_logs.find(match_qry).toArray(function (err, call_log) {
        console.log("call_log" + JSON.stringify(call_log));
        if (call_log.length > 0) {
            var update_logs = {
                comment: retailer_data.remarks || "",
                call_type: "Loyalty Survey" || "",
                call_status: retailer_data.call_status || "",
                created_by: retailer_data.user_id || "",
                created_date: new Date()
            }
            col_call_logs.update(match_qry, {$push: {call_logs: update_logs}}, {multi: false}, function (err, result) {
                if (err) {
                    logger.info("Error in Updating Retailer Data,err:" + err.message);
                    // util.send_to_response({
                    //     result_value: 0,
                    //     msg: 'Error in Updating Retailer Data',
                    //     err: err
                    // }, res);
                    callback(ex, null);
                } else {
                    logger.info("Retailer logs updated Successfully.");
                    //Close connection
                    //db.close();
                    // util.send_to_response({
                    //     response_code: 1,
                    //     msg: "Retailer Call Completed Successfully."
                    // }, res);
                    callback(null, "Updated  Sucesfully");
                }
            });
        }
        else {

            var update_logs = {
                comment: retailer_data.remarks || "",
                call_type: "Loyalty Survey" || "",
                call_status: retailer_data.call_status || "",
                created_by: retailer_data.user_id || "",
                created_date: new Date()
            }
            col_call_logs.insert({
                retailer_code: retailer_data.retailer_code,
                call_logs: [update_logs]
            }), function (err, insert_result) {
                if (insert_result) {


                    logger.info("Sucessfully inserted the data");
                    callback(null, "Inserted Sucessfully");

                }
                else {
                    logger.info("Error in inserting the data" + err);
                    callback(ex, null);
                }

            }
        }

    })
};
//Update retailer data End

//get callers screen data start
exports.get_callers_screen_data = function (req, res) {
//console.log(req.body.skip)
    // console.log(req.body.limit)
    var call_status = req.body.call_status;
    var call_type = req.body.call_type;
    var s = req.body.skip;
    var l = req.body.limit;
    var retailerQuery = {};

    if (call_status) {
        if (call_status == "Pending") {
            retailerQuery = {
                "survey_auto_status": "Pending",
                "call_type": call_type,
                "call_status": "Fresh",
                "active": 1
            };
        }
        else if (call_status == "Complete") {
            retailerQuery = {
                "survey_auto_status": "Complete",
                "call_type": call_type,
                "call_status": "Survey Done",
                "active": 1
            };
        }
        else if (call_status == "WIP") {
            retailerQuery = {
                "survey_auto_status": "Pending",
                "call_type": call_type,
                "call_status": {$in: ["RNR", "Call Back"]},
                "active": 1
            };
        }
        else if (call_status == "Ignore") {
            retailerQuery = {
                "survey_auto_status": "Complete",
                "call_type": call_type,
                "call_status": {$in: ["Wrong Number", "No Shop", "Invalid Shop", "Do Not Want To Participate"]},
                "active": 1
            };
        }
        else {

        }

    }

    //Initialize User Collection
    var col_user = serverDB.collection('retailer_master');

    console.log("retailerQuery : " + JSON.stringify(retailerQuery));

    col_user.find(retailerQuery).skip(s).limit(l).toArray(function (err, retailer_result) {
        if (err) {
            //logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching Retailer Data.', error_msg: err}, res);
        }
        else {

            logger.info("Fetching Retailer Successfully. User Count Is :  " + retailer_result.length);
            if (retailer_result.length > 0) {
                util.send_to_response({
                    response_code: 1,
                    msg: 'Fetching retailer data for calling successfully.',
                    data: retailer_result
                }, res);
            }
            else {
                util.send_to_response({
                    response_code: 2,
                    msg: 'There are no any data for given search type.',
                    data: retailer_result
                }, res);
            }
        }
    });
};
//get callers screen data end


//shalini api
//Insert Caller Login Credentials
exports.insert_user_details = function (req, res) {
    // logger.info("Incoming Body : " + JSON.stringify(req.body));

    var col_user = serverDB.collection('user');
    var find_qry = {"user_id": req.body.user_id};
    col_user.find(find_qry).toArray(function (err, result) {
        if (err) {
            logger.info("Failed to fetch the data : " + err);
        }
        else {
            logger.info("Sucessfully getting the data : " + result.length);
            if (result.length > 0) {
                var col_user_update = serverDB.collection('user');
                var update = {"user_id": req.body.user_id};
                var set_qry = {
                    $set: {
                        "user_password": req.body.user_password,
                        "user_type": req.body.user_type,
                        "user_first_name": req.body.user_first_name,
                        "user_last_name": req.body.user_last_name,
                        "user_middle_name": req.body.user_middle_name,
                        "user_designation": req.body.user_designation,
                        "user_email_id": req.body.user_email_id,
                        "created_by": req.body.created_by,
                        "created_dt": req.body.created_dt
                    }
                };
                col_user_update.update(update, set_qry), function (err, res) {
                    if (err) {

                        logger.info("Error in getting the data" + err);
                    }
                    else {
                        logger.info("Sucessfully updated the data");
                        db.close();
                    }

                }
            }
            else {
                var col_user_insert = serverDB.collection('user');
                var insert_caller_data = req.body
                var insert_qry = insert_caller_data;
                col_user_insert.insert(insert_qry), function (err, insert_result) {
                    if (err) {

                        logger.info("Error in inserting the data" + err);
                    }
                    else {
                        logger.info("Sucessfully inserted the data");
                        db.close();
                    }

                }
            }
        }
        util.send_to_response("Caller Login Credentials Created Sucessfully", res)

    })

}


//GET API FOR CALLER DETIALS START
exports.get_exsisting_data_caller = function (req, res) {
    var con_user_tbl = serverDB.collection('user');
    con_user_tbl.find({}).toArray(function (err, result) {
        if (err) {
            logger.info("Error in getting the data" + err);
        }
        else {
            logger.info("Sucessfully Getting the data " + result.length);

        }
        util.send_to_response({
            response_code: 1,
            msg: "Caller Data.",
            data: result
        }, res);
    })

}
//GET API FOR CALLER DETIALS ENDS


//API TO insert THE SUPER ADMIN QUESTION

exports.insert_super_admin_question = function (req, res) {
    console.log("incoming" + JSON.stringify(req.body));
    var con_question_tbl = serverDB.collection('feedback_question');
    con_question_tbl.insert(req.body, {w: 1, keepGoing: true}, function (err, result) {
        if (err) {
            logger.info("Error in inserting the data" + err);
        }
        else {
            logger.info("Sucessfully inserted the data");
            // db.close();
            util.send_to_response("FeedBack Question Inserted Sucessfully", res)
        }

    })

}

//API TO GET FEEDBACK QUESTION

exports.get_feedback_question = function (req, res) {
    // console.log("incoming"+JSON.stringify(req.body));
    var con_question_tbl = serverDB.collection('feedback_question');
    var pipeline = [{
        $match: {
            "active": 1,
            rank: {$exists: true}
        }
    },
        {
            $sort: {rank: 1}
        },
        {
            $project: {
                "question": 1,
                "created_by": 1,
                "rank": 1
            }
        }]
    con_question_tbl.aggregate(pipeline).toArray(function (err, result) {
        if (err) {
            console.log("error in fetching" + err)
        }
        else {
            console.log("suceesfuly" + result);
            util.send_to_response(result, res);
        }
    })


}

//Update retailer data start
exports.complete_retailer_call = function (req, res) {

    var retailer_code = req.body.retailer_code;
    var call_status = req.body.call_status;
    var remarks = req.body.remarks;
    var user_data = req.body.user_data;

    if (typeof retailer_code === 'undefined' || retailer_code == "" || retailer_code == "") {
        logger.info("Retailer Code Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Retailer Code Is Not Supplied'}, res);
        return;
    }

    if (typeof call_status === 'undefined' || call_status == "" || call_status == "") {
        logger.info("Call Status Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Call Status Is Not Supplied'}, res);
        return;
    }

    if (typeof remarks === 'undefined' || remarks == "" || remarks == "") {
        logger.info("Remarks Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Remarks Is Not Supplied'}, res);
        return;
    }

    var update_query = {retailer_code: retailer_code};

    var update_data = {};

    if (call_status == "RNR" || call_status == "Call Back") {
        update_data = {
            $set: {
                survey_auto_status: "Pending",
                call_status: call_status,
                remarks: remarks,
                wip_user: user_data,
                wip_ts: new Date()
            }
        };
    }
    else if (call_status == "Wrong Number" || call_status == "No Shop" || call_status == "Invalid Shop" || call_status == "Do Not Want To Participate") {
        update_data = {
            $set: {
                survey_auto_status: "Complete",
                call_status: call_status,
                remarks: remarks,
                wip_user: user_data,
                wip_ts: new Date(),
                stop_ts: new Date()
            }
        };
    }
    else if (call_status == "Survey Done") {
        update_data = {
            $set: {
                survey_auto_status: "Complete",
                call_status: call_status,
                remarks: remarks,
                wip_user: user_data,
                wip_ts: new Date(),
                stop_ts: new Date()
            }
        };
    }

    //Initialize retailer_master Collection
    var col_retailer_master = serverDB.collection('retailer_master');

    col_retailer_master.update(update_query, update_data, {multi: false}, function (err, result) {
        if (err) {
            logger.info("Error in Updating Retailer Data,err:" + err.message);
            util.send_to_response({
                result_value: 0,
                msg: 'Error in Updating Retailer Data',
                err: err
            }, res);
        } else {

            logger.info("Retailer data is updated in retailer-master successfully.");

            var col_call_logs = serverDB.collection('call_logs');

            col_call_logs.find({retailer_code: retailer_code}).toArray(function (err, logs_result) {
                if (err) {
                    logger.info("Error In Fetching logs data,err:" + err.message);
                    util.send_to_response({response_code: 0, msg: 'Error In Fetching Logs Data.', error_msg: err}, res);
                }
                else {

                    var update_logs = {
                        comment: remarks,
                        call_type: "Loyalty Survey",
                        call_status: call_status,
                        created_by: user_data.user_id,
                        created_date: new Date()
                    };

                    if (logs_result.length > 0) {

                        col_call_logs.update(update_query, {$push: {call_logs: update_logs}}, {multi: false}, function (err, result) {
                            if (err) {
                                logger.info("Error in Updating Retailer Data,err:" + err.message);
                                util.send_to_response({
                                    result_value: 0,
                                    msg: 'Error in Updating Retailer Data',
                                    err: err
                                }, res);
                            } else {
                                logger.info("Retailer logs updated Successfully.");
                                //Close connection
                                //db.close();
                                util.send_to_response({
                                    response_code: 1,
                                    msg: "Retailer Call Completed Successfully."
                                }, res);
                            }
                        });
                    }
                    else {
                        col_call_logs.insert({retailer_code: retailer_code, call_logs: [update_logs]}, {
                            w: 1,
                            keepGoing: true
                        }, function (err, result) {
                            if (err) {
                                logger.info("Error in Inserting Allocation,err:" + err.message);

                            } else {
                                logger.info("Retailer logs inserted Successfully.");
                                util.send_to_response({
                                    response_code: 1,
                                    msg: "Retailer Call Completed Successfully."
                                }, res);
                            }
                        });
                    }

                }
            });
        }
    });

};
//Update retailer data End

//Update retailer data start
exports.survey_data_update = function (req, res) {
    // console.log("movi" + JSON.stringify(req.body));
    var question = [];
    // console.log("survey" + JSON.stringify(req.body.survey));

    var col_retailer_master = serverDB.collection('retailer_master');
    var update_query = {retailer_code: req.body.retailer_code};

    col_retailer_master.update(update_query, {
        $set: {
            survey: req.body.survey,
            "movie_ticket_status": req.body.movie_tickets_status,
            "survey_auto_status": "Complete",
            "call_status": "Survey Done"

        }
    }, {multi: false}, function (err, result) {
        if (err) {
            logger.info("Error in Updating Retailer Data,err:" + err.message);
            util.send_to_response({
                result_value: 0,
                msg: 'Error in Updating Retailer Data',
                err: err
            }, res);
        } else {
            logger.info("Retailer logs updated Successfully.");

            var update_data = {
                $set: {
                    survey_auto_status: "Complete",
                    call_status: "Survey Done",
                    remarks: req.body.remarks,
                    wip_user: req.body.user_data,
                    wip_ts: new Date(),
                    stop_ts: new Date()
                }
            };

            col_retailer_master.update(update_query, update_data, {multi: false}, function (err, result) {
                if (err) {
                    logger.info("Error in Updating Retailer Data,err:" + err.message);
                    util.send_to_response({
                        result_value: 0,
                        msg: 'Error in Updating Retailer Data',
                        err: err
                    }, res);
                }
                else {

                    var col_call_logs = serverDB.collection('call_logs');

                    col_call_logs.find({retailer_code: req.body.retailer_code}).toArray(function (err, logs_result) {
                        if (err) {
                            logger.info("Error In Fetching logs data,err:" + err.message);
                            util.send_to_response({
                                response_code: 0,
                                msg: 'Error In Fetching Logs Data.',
                                error_msg: err
                            }, res);
                        }
                        else {
                            var update_logs = {
                                comment: req.body.remarks,
                                call_type: "Loyalty Survey",
                                call_status: "Survey Done",
                                created_by: req.body.user_data.user_id,
                                created_date: new Date()
                            };

                            if (logs_result.length > 0) {
                                col_call_logs.update(update_query, {$push: {call_logs: update_logs}}, {multi: false}, function (err, result) {
                                    if (err) {
                                        logger.info("Error in Updating Retailer Data,err:" + err.message);
                                        util.send_to_response({
                                            result_value: 0,
                                            msg: 'Error in Updating Retailer Data',
                                            err: err
                                        }, res);
                                    } else {
                                        logger.info("Retailer logs updated Successfully.");
                                        //Close connection
                                        //db.close();
                                        util.send_to_response({
                                            response_code: 1,
                                            msg: "Loyalty Survey Updated Successfully."
                                        }, res);
                                    }
                                });
                            }
                            else {
                                col_call_logs.insert({
                                    retailer_code: req.body.retailer_code,
                                    call_logs: [update_logs]
                                }, {
                                    w: 1,
                                    keepGoing: true
                                }, function (err, result) {
                                    if (err) {
                                        logger.info("Error in Inserting Retailer logs,err:" + err.message);
                                    } else {
                                        logger.info("Retailer logs inserted Successfully.");
                                        util.send_to_response({
                                            response_code: 1,
                                            msg: "Loyalty Survey Updated Successfully."
                                        }, res);
                                    }
                                });
                            }
                        }
                    });
                }
            })
        }
    });
};
//Update retailer data End

//Login For user Start
exports.get_retailer_data = function (req, res) {

    var retailer_code = req.body.retailer_code;

    if (typeof retailer_code === 'undefined' || retailer_code == "" || retailer_code == "") {
        logger.info("User Id Is Not Supplied");
        util.send_to_response({response_code: 0, msg: 'Retailer Code Not Supplied'}, res);
        return;
    }

    logger.info("Incoming Body : " + JSON.stringify(req.body));

    //Initialize User Collection
    var col_retailer_master = serverDB.collection('retailer_master');

    col_retailer_master.find({retailer_code: req.body.retailer_code}).toArray(function (err, user_result) {
        if (err) {
            logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching User Data.', error_msg: err}, res);
        }
        else {
            logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);

            util.send_to_response({
                response_code: 1,
                msg: "Retailer Data.",
                data: user_result
            }, res);

        }
    });
};
//Login For user End


//API to search the field starts
exports.get_data_by_search_input = function (req, res) {

    // console.log("incoming body" + JSON.stringify(req.body));
    var input_data = req.body.regex_text;
    var retailer_con = serverDB.collection('retailer_master');
    var call_type = req.body.call_type;
    // console.log("ca"+call_type);
    if (call_type == "Pending") {
        var survey_auto_status = "Pending";
        var call_status = ["Fresh"];
    }
    if (call_type == "WIP") {
        var survey_auto_status = "Pending";
        var call_status = ["RNR", "Call Back"];
    }
    if (call_type == "Ignore") {
        var survey_auto_status = "Complete";
        var call_status = ["Wrong Number", "No Shop", "Invalid Shop", "Do Not Want To Participate"];
    }
    if (call_type == "Complete") {
        var survey_auto_status = "Complete";
        var call_status = ["Survey Done"];
    }
    // console.log("sd"+call_status);
    var regex_query = {

        "survey_auto_status": survey_auto_status,
        "call_type": "Retailer Loyalty Survey",
        "call_status": {$in: call_status},
        $or: [{"retailer_name": {$regex: input_data, $options: '-i'}},
            {"retailer_registered_mobile": {$regex: input_data}},
            {"outlet_name": {$regex: input_data, $options: '-i'}},
            {"retailer_address": {$regex: input_data, $options: '-i'}},
            {"remarks": {$regex: input_data, $options: '-i'}},
            {"wip_user.user_id": {$regex: input_data, $options: '-i'}},
            {"call_status": {$regex: input_data, $options: '-i'}}
        ]
    }
    retailer_con.find(regex_query).toArray(function (err, user_result) {
        if (user_result) {
            // console.log("result" + JSON.stringify(user_result.length));
            util.send_to_response({
                response_code: 1,
                msg: "Search_data",
                data: user_result
            }, res);
        }
        else {
            // console.log("did not get the data");
            util.send_to_response({
                response_code: 0,
                msg: "No Data Found",
                data: user_result
            }, res);
        }

    })


};

//API to search the field ends

//get wip user list
exports.get_wip_user_list = function (req, res) {

    var pipeline = [
        {
            $match: {
                wip_user: {$exists: true}
            }
        },
        {
            $group: {
                _id: {
                    'wip_user_user_id': '$wip_user.user_id'
                },
                total_count: {$sum: 1}
            }
        },
        {
            $project: {
                _id: 0,
                user_id: '$_id.wip_user_user_id',
                total_count: '$total_count'
            }
        }
    ];

    var col_retailer_master = serverDB.collection('retailer_master');

    col_retailer_master.aggregate(pipeline).toArray(function (err, user_result) {
        if (err) {
            logger.info("Error In Fetching User Data. " + err.message);
            //Close connection
            db.close();
        }
        else {
            logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);
            logger.info("Fetching User Data Successfully. User :  " + JSON.stringify(user_result));

            util.send_to_response({
                response_code: 1,
                msg: "WIP User Data.",
                data: user_result
            }, res);
        }
    });
};

//get wip user list
exports.get_wip_user_data = function (req, res) {

    console.log(JSON.stringify(req.body));

    var userDataQuery = {
        "survey_auto_status": "Pending",
        'wip_user.user_id': req.body.user_id,
        "call_status": {$in: ["RNR", "Call Back"]},
        "active": 1
    };

    var col_retailer_master = serverDB.collection('retailer_master');
    var l = req.body.limit;
    var s = req.body.skip;
    col_retailer_master.find(userDataQuery).limit(l).skip(s).toArray(function (err, user_result) {
        if (err) {
            logger.info("Error In Fetching User Data,err:" + err.message);
            util.send_to_response({response_code: 0, msg: 'Error In Fetching User Data.', error_msg: err}, res);
        }
        else {
            logger.info("Fetching User Data Successfully. User Count Is :  " + user_result.length);

            util.send_to_response({
                response_code: 1,
                msg: "User wise calling data.",
                data: user_result
            }, res);

        }
    });

};

// ******************* private helper functions ***********************
var send_to_response = function (results, res) {
    res.contentType('application/json');
    if (util.is_array(results)) {
        var arr = [];
        results.forEach(function (r) {
            arr.push(r)
        });
        res.send(arr);
    } else {

        res.send(results);
    }
};
var return_back = function (results) {
    var arr = [];
    results.forEach(function (claim) {
        arr.push(claim)
    });
    return arr;
};
var send_empty_recordset = function (res) {
    var response = {"message": "no result found", "length": 0};
    res.contentType('application/json');
    res.send(response);
};

// ***********************************************************************
//limit and skip functionality
//Below functions are to be used by every function to ensure
//limit and skip values are legal
//and no error is generated as a result.
//Every developer needs to use these if they are implementing
// ***********************************************************************
var sanitize_limit = function (req) {
    var limit = req.params.limit;
    if ((limit === undefined) || (limit < 0)) {
        limit = util.get_fetch_limit();
    }
    return limit;
}
var sanitize_skip = function (req) {
    var skip = req.params.skip;
    if ((skip === undefined) || (skip < 0)) {
        skip = 0;
    }
    return skip;
}
// ***********************************************************************


this.renew_token = function (req, res, next) {
    var token_data = token.verify(req.headers.token);
    var token_renw = "";
    if (token_data) {
        token_renw = token.assign(token_data);
    }
    util.send_to_response(token_renw, res);

}


// ******************* private helper functions ***********************
