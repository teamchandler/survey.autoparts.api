/**
 * Created by Mahiruddin Seikh on October 18,2016.
 */
var config = require('./config/config.json'),
    _ = require('lodash-node')
    ;
log4js = require('log4js');

log4js.configure('./config/log4js.json', {});

exports.get_logger = function (log) {
    var logger = log4js.getLogger(log);
    logger.setLevel(config.logging_level);
    return logger;
}

exports.get_listening_port = function () {
    return config.port;
}

exports.get_program_name = function (app_key) {
    return config.program_name;
}

exports.get_connection_string = function (product) {
    var e = config.env;
    var connection_string;
    if (product == "mongo") {
        var address = _.find(config.mongo_address, function (env) {
            return env.env == e;
        });
        connection_string = "mongodb://" + address.mongo_user_id + ":" + address.mongo_password + "@" + address.ip + "/" + address.db + "?poolSize=4" + "&authSource=admin";
    }
    return connection_string;
}

exports.is_array = function (value) {
    return Object.prototype.toString.call(value) === '[object Array]';
}

exports.create_routing_log = function (method, url, prefix, route) {
    var tmp = "Route Sub System: " + route + "\n" + "Method: " + method + "\n" + "URL:" + "/" + prefix + url;
    return tmp;
}

exports.get_source_by_query_purpose = function (query_purpose) {
    var source = _.find(config.query_setup, function (q) {
        return (q.purpose == query_purpose);
    });
    return source;
}

exports.send_to_response = function (results, res) {
    if (results instanceof Array) {
        var arr = [];
        results.forEach(function (r) {
            arr.push(r)
        });
    }
    else {
        if (typeof results === 'number') {
            var arr = {msg: results };
        }
        else {
            var arr = results;
        }
    }
    res.contentType('application/json');
    res.send(arr);
}

exports.get_audit_trail_config = function () {
    return config.audit_trail;
}

exports.get_fetch_limit = function () {
    return config.fetch_limit;
}




